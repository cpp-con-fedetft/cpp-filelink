
#include "crow.h"
#include "mstch/mstch.hpp"

using namespace std;
using namespace crow;

const int key=
#include "key"
;

int main()
{
    SimpleApp srv;
    string ip;
    mustache::set_base("..");

    CROW_ROUTE(srv, "/")([&](){
		std::string view{"{{#names}}Hi {{name}}!\n{{/names}}"};
		mstch::map context{
			{"names", mstch::array{
			mstch::map{{"name", std::string{"Chris"}}},
			mstch::map{{"name", std::string{"Mark"}}},
			mstch::map{{"name", std::string{"Scott"}}},
			}}
		};
		return mstch::render(view, context);
// 		mstch::context ctx;
// 		ctx["test"]="foo";
//         return mustache::load("index.html").render(ctx);
    });

    CROW_ROUTE(srv, "/minecraft/<int>")([&](const request& req, int k){
        if(k==key) ip=req.endpoint.address().to_string();
        return "";
    });

    CROW_ROUTE(srv, "/minecraft")([&](){
        return ip;
    });

    srv.port(8080).run();
}
